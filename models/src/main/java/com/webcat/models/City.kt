package com.webcat.models

import com.google.gson.annotations.SerializedName

data class City constructor(

    var id: Int? = null,
    var name: String? = null,
    var state: String? = null,
    var country: String? = null,


    )


