package com.webcat.models.errors

class NetworkException(message: String) : Exception(message) {
}