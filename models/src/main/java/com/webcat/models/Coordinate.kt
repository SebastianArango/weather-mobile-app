package com.webcat.models

import java.io.Serializable

data class Coordinate constructor(val lon: String, val lat: String) : Serializable