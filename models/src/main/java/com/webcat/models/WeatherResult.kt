package com.webcat.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class WeatherResult constructor(
    val base: String? = null,
    @SerializedName("clouds")
    val clouds: Clouds? = null,
    val cod: Int? = null,
    @SerializedName("coord")
    var coordinate: Coordinate? = null,
    val dt: Int? = null,
    val id: Int? = null,
    @SerializedName("main")
    val main: Main? = null,
    val name: String? = null,
    @SerializedName("sys")
    val sys: Sys? = null,
    val timezone: Int? = null,
    val visibility: Int? = null,
    @SerializedName("weather")
    val weather: List<Weather>? = null,
    @SerializedName("wind")
    val wind: Wind? = null,
) : Serializable