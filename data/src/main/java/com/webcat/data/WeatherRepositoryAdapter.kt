package com.webcat.data

import com.webcat.domain.WeatherRepository
import com.webcat.models.WeatherResult
import javax.inject.Inject

class WeatherRepositoryAdapter @Inject constructor(private val weatherApiSource: WeatherApiSource): WeatherRepository  {
    override suspend fun getWeatherByLocalization(lat: String, lon: String): WeatherResult {
       return weatherApiSource.getWeatherByLocalization(lat, lon)
    }

    override suspend fun getWeatherByCityName(cityName: String): WeatherResult {
        return weatherApiSource.getWeatherByCityName(cityName)
    }
}