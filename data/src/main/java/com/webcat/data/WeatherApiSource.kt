package com.webcat.data

import com.webcat.models.WeatherResult


interface WeatherApiSource {
    suspend fun getWeatherByLocalization(lat: String, lon: String): WeatherResult
    suspend fun getWeatherByCityName(cityName: String): WeatherResult
}