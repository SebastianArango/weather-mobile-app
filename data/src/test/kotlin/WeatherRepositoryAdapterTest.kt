import com.webcat.data.WeatherApiSource
import com.webcat.data.WeatherRepositoryAdapter
import com.webcat.models.WeatherResult
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class WeatherRepositoryAdapterTest {

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var weatherApiSource: WeatherApiSource
    private lateinit var repositoryAdapter: WeatherRepositoryAdapter

    @Before
    fun setUp() {
        repositoryAdapter = WeatherRepositoryAdapter(weatherApiSource)
    }

    @Test
    fun getWeatherByCityNameShouldReturnDataFromApiSource() {
        testCoroutineRule.runBlockingTest {
            val cityName = "Medellin"
            val expected = WeatherResult(cod = 12134)
            doReturn(expected).`when`(weatherApiSource).getWeatherByCityName(cityName)
            val result = repositoryAdapter.getWeatherByCityName(cityName)
            verify(weatherApiSource).getWeatherByCityName(cityName)
            Assert.assertEquals(result.cod, expected.cod)
        }
    }

    @Test
    fun getWeatherByLocationNameShouldReturnDataFromApiSource() {
        testCoroutineRule.runBlockingTest {
            val latitude = "6.2518"
            val longitude = "-75.5636"
            val expected = WeatherResult(cod = 12134)
            doReturn(expected).`when`(weatherApiSource).getWeatherByLocalization(latitude, longitude)
            val result = repositoryAdapter.getWeatherByLocalization(latitude, longitude)
            verify(weatherApiSource).getWeatherByLocalization(latitude, longitude)
            Assert.assertEquals(result.cod, expected.cod)
        }
    }

}



