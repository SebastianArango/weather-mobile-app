
import com.webcat.domain.WeatherRepository
import com.webcat.domain.WeatherUseCaseAdapter
import com.webcat.models.WeatherResult
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class WeatherUseCaseAdapterTest {

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var weatherRepository: WeatherRepository
    private lateinit var weatherUseCaseAdapter: WeatherUseCaseAdapter

    @Before
    fun setUp() {
        weatherUseCaseAdapter = WeatherUseCaseAdapter(weatherRepository)
    }

    @Test
    fun getWeatherByCityNameByShouldReturnDataFromRepository() {
        testCoroutineRule.runBlockingTest {
            val cityName = "Medellin"
            val expected = WeatherResult(cod = 12134)
            doReturn(expected).`when`(weatherRepository).getWeatherByCityName(cityName)
            val result = weatherUseCaseAdapter.getWeatherByCityName(cityName)
            verify(weatherRepository).getWeatherByCityName(cityName)
            Assert.assertEquals(result.cod, expected.cod)
        }
    }

    @Test
    fun getWeatherByLocationNameByShouldReturnDataFromRepository() {
        testCoroutineRule.runBlockingTest {
            val latitude = "6.2518"
            val longitude = "-75.5636"
            val expected = WeatherResult(cod = 12134)
            doReturn(expected).`when`(weatherRepository).getWeatherByLocalization(latitude, longitude)
            val result = weatherUseCaseAdapter.getWeatherByLocalization(latitude, longitude)
            verify(weatherRepository).getWeatherByLocalization(latitude, longitude)
            Assert.assertEquals(result.cod, expected.cod)
        }
    }
}