package com.webcat.domain

import com.webcat.models.WeatherResult


interface WeatherUseCase {
    @Throws(Exception::class)
    suspend fun getWeatherByLocalization(lat: String, lon: String): WeatherResult
    suspend fun getWeatherByCityName(cityName: String): WeatherResult
}