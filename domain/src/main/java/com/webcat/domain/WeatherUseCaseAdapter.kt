package com.webcat.domain

import com.webcat.models.WeatherResult
import javax.inject.Inject

class WeatherUseCaseAdapter @Inject constructor(private val weatherRepository: WeatherRepository) :
    WeatherUseCase {
    override suspend fun getWeatherByLocalization(lat: String, lon: String): WeatherResult {
        return weatherRepository.getWeatherByLocalization(lat, lon)
    }

    override suspend fun getWeatherByCityName(cityName: String): WeatherResult {
        return weatherRepository.getWeatherByCityName(cityName)
    }
}