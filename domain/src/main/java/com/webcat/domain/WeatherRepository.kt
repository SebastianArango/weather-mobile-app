package com.webcat.domain

import com.webcat.models.WeatherResult

interface WeatherRepository {
    suspend fun getWeatherByLocalization(lat: String, lon: String): WeatherResult
    suspend fun getWeatherByCityName(cityName: String): WeatherResult
}