package com.webcat.weatherapp

import android.app.Application
import android.util.Log
import com.facebook.stetho.Stetho
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class WeatherApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)

        val oldHandler = Thread.getDefaultUncaughtExceptionHandler()
        Thread.setDefaultUncaughtExceptionHandler { thread, throwable ->
            Log.e("WeatherApplication", throwable.message, throwable)
            oldHandler?.uncaughtException(thread, throwable)
        }
    }
}