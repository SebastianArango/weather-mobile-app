package com.webcat.weatherapp.di

import com.webcat.data.WeatherRepositoryAdapter
import com.webcat.domain.WeatherRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoriesModule {

    @Binds
    abstract fun bindWeatherRepository(
        weatherRepository: WeatherRepositoryAdapter
    ):WeatherRepository
}