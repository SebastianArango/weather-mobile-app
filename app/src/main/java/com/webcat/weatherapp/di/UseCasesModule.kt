package com.webcat.weatherapp.di

import com.webcat.domain.WeatherUseCase
import com.webcat.domain.WeatherUseCaseAdapter
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class UseCasesModule {

    @Binds
    abstract fun bindWeatherUseCase(
        weatherUseCase: WeatherUseCaseAdapter
    ):WeatherUseCase
}