package com.webcat.weatherapp.di

import com.webcat.data.WeatherApiSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import com.webcat.weatherapp.datasource.api_source.WeatherApiSourceAdapter

@Module
@InstallIn(SingletonComponent::class)
abstract class DataSourcesModule {

    @Binds
    abstract fun bindWeatherApiSource(
        weatherApiSource: WeatherApiSourceAdapter
    ):WeatherApiSource
}