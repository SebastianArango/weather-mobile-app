package com.webcat.weatherapp.datasource.api_source


import com.webcat.models.WeatherResult
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherServices {
    @GET("weather?appid=7c9f59b55e57c4ec917f60e4b1ea6e4a")
    suspend fun getWeatherByLocalization(
        @Query("lat") lat: String,
        @Query("lon") lon: String
    ): Response<WeatherResult>

    @GET("weather?appid=7c9f59b55e57c4ec917f60e4b1ea6e4a")
    suspend fun getWeatherByCityName(
        @Query("q") cityName: String
    ): Response<WeatherResult>
}