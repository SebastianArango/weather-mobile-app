package com.webcat.weatherapp.datasource.api_source

import com.webcat.data.WeatherApiSource

import com.webcat.models.errors.NetworkException
import com.webcat.models.WeatherResult
import com.webcat.weatherapp.utils.ExceptionFactory
import javax.inject.Inject

class WeatherApiSourceAdapter @Inject constructor(private val weatherServices: WeatherServices) :
    WeatherApiSource {

    override suspend fun getWeatherByLocalization(lat: String, lon: String): WeatherResult {
        try {
            val response = weatherServices.getWeatherByLocalization(lat, lon)
            if (response.isSuccessful) {
                return response.body()!!
            }
            throw NetworkException("http error! ${response.code()}")
        } catch (e: Exception) {
            throw ExceptionFactory.resolveError(e)
        }
    }

    override suspend fun getWeatherByCityName(cityName: String): WeatherResult {
        try {
            val response = weatherServices.getWeatherByCityName(cityName)
            if (response.isSuccessful) {
                return response.body()!!
            }
            throw NetworkException("http error! ${response.code()}")
        } catch (e: Exception) {
            throw ExceptionFactory.resolveError(e)
        }
    }
}