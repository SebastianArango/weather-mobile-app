package com.webcat.weatherapp.ui.adapter

import android.annotation.SuppressLint
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.webcat.models.City
import com.webcat.weatherapp.databinding.ItemCityBinding


class CitiesViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val binding = ItemCityBinding.bind(view)

    @SuppressLint("SetTextI18n")
    fun render(city: City, eventOnclickListener: (City) -> Unit) {
        binding.textViewCityName.text = city.name
        binding.textViewState.text = city.state
        binding.textViewCountry.text = "Country: ${city.country}"

        itemView.setOnClickListener { eventOnclickListener(city) }

    }
}