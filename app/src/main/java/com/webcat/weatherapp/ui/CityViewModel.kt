package com.webcat.weatherapp.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.webcat.domain.WeatherUseCase
import com.webcat.models.WeatherResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CityViewModel @Inject constructor(private val weatherUseCase: WeatherUseCase) :
    ViewModel() {

    private val _weatherResult = MutableLiveData<WeatherResult>()
    val weatherResult: LiveData<WeatherResult> = _weatherResult
    private val _showCircularProgressIndicator = MutableLiveData<Boolean>()
    val showCircularProgressIndicator: LiveData<Boolean> = _showCircularProgressIndicator
    private val _error = MutableLiveData<Exception>()
    val error: LiveData<Exception> = _error

    fun getWeatherByCityName(cityName: String) {
        viewModelScope.launch {
            _showCircularProgressIndicator.value = true
            try {
                val result = weatherUseCase.getWeatherByCityName(cityName)
                _weatherResult.value = result
            } catch (e: Exception) {
                _error.value = e
            } finally {
                _showCircularProgressIndicator.value = false
            }
        }
    }

    fun getWeatherByLocalization(lat: String, lon: String) {
        viewModelScope.launch {
            _showCircularProgressIndicator.value = true
            try {
                val result = weatherUseCase.getWeatherByLocalization(lat, lon)
                _weatherResult.value = result
            } catch (e: Exception) {
                _error.value = e
            } finally {
                _showCircularProgressIndicator.value = false
            }
        }
    }
}