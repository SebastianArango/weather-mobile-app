package com.webcat.weatherapp.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import com.webcat.weatherapp.R
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.webcat.models.City
import com.webcat.models.WeatherResult
import com.google.android.gms.location.*
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import com.webcat.models.errors.NetworkException
import com.webcat.weatherapp.databinding.ActivityCityBinding
import com.webcat.weatherapp.ui.adapter.CitiesAdapter
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException


@AndroidEntryPoint
class CityActivity : AppCompatActivity() {

    private val cityViewModel: CityViewModel by viewModels()
    private lateinit var binding: ActivityCityBinding

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private val permissionRequestCode: Int = 99
    private var requestPermissionFromApp: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initRecyclerView()
        createObserverCircularProgress()
        createObserverResult()
        observerError()
        initEventOnclickFloatingActionButton()
    }

    override fun onResume() {
        super.onResume()

        if (requestPermissionFromApp) {
            requestPermissionFromApp = !requestPermissionFromApp
            if (checkPermissions()) {
                readCurrentLocation()
            } else {
                showToast(R.string.alertMessage)
            }
        }
    }

    private fun observerError() {
        cityViewModel.error.observe(this, {
            Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
        })
    }

    private fun createObserverCircularProgress() {
        cityViewModel.showCircularProgressIndicator.observe(this, {
            if (it) {
                binding.progressCircular.visibility = View.VISIBLE
            } else {
                binding.progressCircular.visibility = View.GONE
            }
        })
    }

    private fun initRecyclerView() {
        binding.recyclerViewCities.layoutManager = LinearLayoutManager(this)
        binding.recyclerViewCities.adapter =
            CitiesAdapter(getDataCities()) { city -> onItemSelected(city) }
    }

    private fun getDataCities(): List<City> {
        lateinit var jsonString: String
        try {
            jsonString = this.assets.open("city_list.json")
                .bufferedReader()
                .use { it.readText() }
        } catch (ioException: IOException) {
            print(ioException)
        }

        val listCountryType = object : TypeToken<List<City>>() {}.type
        return Gson().fromJson(jsonString, listCountryType)
    }

    private fun onItemSelected(city: City) {
        cityViewModel.getWeatherByCityName(city.name!!)
    }

    private fun createObserverResult() {
        cityViewModel.weatherResult.observe(this, Observer {
            it?.let {
                validateResponse(it)
            }
        })
    }

    private fun validateResponse(weatherResult: WeatherResult) {
        weatherResult.base.let {
            val intent = Intent(this, CityDetailActivity::class.java).apply {
                putExtra("weatherData", weatherResult)
            }
            startActivity(intent)
        }
    }

    private fun initEventOnclickFloatingActionButton() {
        binding.floatingActionButtonLocation.setOnClickListener {
            if (checkPermissions()) {
                readCurrentLocation()
            } else {
                callRequestPermission()
            }
        }
    }

    private fun callRequestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            permissionRequestCode
        )
    }

    companion object {
        private val REQUIRED_PERMISSIONS_GPS = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }

    private fun checkPermissions() = REQUIRED_PERMISSIONS_GPS.all {
        ContextCompat.checkSelfPermission(baseContext, it) == PackageManager.PERMISSION_GRANTED
    }

    @SuppressLint("MissingPermission")
    private fun readCurrentLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        if (isLocationEnabled()) {
            fusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                val location: Location? = task.result
                if (location == null) {
                    requestNewLocationData()
                } else {
                    callServiceByLocation(
                        location.latitude.toString(),
                        location.longitude.toString()
                    )
                }
            }
        } else {
            showAlertDialog()
        }
    }

    private fun showAlertDialog() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        val dialog: AlertDialog = builder.setTitle(R.string.alertTitle)
            .setMessage(R.string.alertMessage)
            .setPositiveButton(R.string.alertButtonPositive) { dialog, _ ->
                dialog.dismiss()
                requestPermissionFromApp = true
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
            .setNegativeButton(R.string.alertButtonNegative) { dialog, _ -> dialog.dismiss() }
            .create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.DKGRAY)

    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 0
        locationRequest.fastestInterval = 0
        locationRequest.numUpdates = 1
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            mLocationCallBack,
            Looper.myLooper()
        )
    }

    private val mLocationCallBack = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val lastLocation: Location = locationResult.lastLocation
            callServiceByLocation(
                lastLocation.latitude.toString(),
                lastLocation.longitude.toString()
            )
        }
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            permissionRequestCode -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    readCurrentLocation()
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    val permission = permissions[0]
                    val showRationale = shouldShowRequestPermissionRationale(permission)

                    if (showRationale) {
                        showToast(R.string.alertMessage)
                    } else {
                        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
                        val dialog: AlertDialog = builder.setTitle(R.string.alertTitle)
                            .setMessage(R.string.alertMessage2)
                            .setPositiveButton("turn on") { dialog, _ ->
                                dialog.dismiss()
                                requestPermissionFromApp = true
                                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                intent.data = Uri.parse("package:$packageName")
                                startActivity(intent)
                            }
                            .setNegativeButton(R.string.alertButtonNegative) { dialog, _ -> dialog.dismiss() }
                            .create()
                        dialog.show()
                        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.DKGRAY)
                    }
                }
                return
            }
        }
    }

    private fun callServiceByLocation(latitude: String, longitude: String) {
        cityViewModel.getWeatherByLocalization(latitude, longitude)
    }

    private fun showToast(message: Int) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}
