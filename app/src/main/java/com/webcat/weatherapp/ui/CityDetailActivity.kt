package com.webcat.weatherapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.webcat.models.WeatherResult
import com.webcat.weatherapp.databinding.ActivityCityDetailBinding

class CityDetailActivity : AppCompatActivity() {


    private lateinit var binding: ActivityCityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val weatherData = intent.getSerializableExtra("weatherData") as WeatherResult

        setData(weatherData)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setData(weatherData: WeatherResult) {
        binding.textViewCityName.text = weatherData.name
        val code = "Code: ${weatherData.cod.toString()}"
        binding.textViewCityId.text = code
        val timeZone = "TimeZone: ${weatherData.timezone.toString()}"
        binding.textViewCityTimeZone.text = timeZone
        var textCoordinates = "Coordinates"
        binding.textViewCoordinate.text = textCoordinates
        val latitude = "Latitude: ${weatherData.coordinate!!.lat}"
        binding.textViewLatitude.text = latitude
        val longitude = "Longitude: ${weatherData.coordinate!!.lon}"
        binding.textViewCityLongitude.text = longitude

        val textWeather = "Weather"
        binding.textViewWeather.text = textWeather
        val main = "Main: ${weatherData.weather!![0].main}"
        binding.textViewMain.text = main
        val description = "Description:\n${weatherData.weather!![0].description}"
        binding.textViewCityDescription.text = description
    }
}