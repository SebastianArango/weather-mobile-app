package com.webcat.weatherapp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.webcat.models.City
import com.webcat.weatherapp.R


class CitiesAdapter(
    private val citiesList: List<City>,
    private val eventOnclickListener: (City) -> Unit
) : RecyclerView.Adapter<CitiesViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CitiesViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        return CitiesViewHolder(layoutInflater.inflate(R.layout.item_city, parent, false))
    }

    override fun onBindViewHolder(holder: CitiesViewHolder, position: Int) {
        val item = citiesList[position]

        holder.render(item, eventOnclickListener)

    }

    override fun getItemCount(): Int =
        citiesList.size


}

