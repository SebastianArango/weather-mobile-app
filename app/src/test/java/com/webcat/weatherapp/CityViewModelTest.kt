package com.webcat.weatherapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.webcat.domain.WeatherUseCase
import com.webcat.models.WeatherResult
import com.webcat.weatherapp.ui.CityViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class CityViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var weatherUseCase: WeatherUseCase

    private lateinit var cityViewModel: CityViewModel

    @Before
    fun setup() {
        cityViewModel = CityViewModel(weatherUseCase)
    }

    @Test
    fun getWeatherByCityNameShouldReturnWeatherResultFromUseCase() {
        testCoroutineRule.runBlockingTest {
            val cityName = "Medellin"
            val expected = WeatherResult(cod = 12134)
            doReturn(expected).`when`(weatherUseCase).getWeatherByCityName(cityName)
            cityViewModel.getWeatherByCityName(cityName)
            verify(weatherUseCase).getWeatherByCityName(cityName)
            Assert.assertEquals(cityViewModel.weatherResult.value, expected)
            Assert.assertEquals(cityViewModel.showCircularProgressIndicator.value, false)
        }
    }

    @Test
    fun getWeatherByLocationNameShouldReturnWeatherResultFromUseCase() {
        testCoroutineRule.runBlockingTest {
            val latitude = "6.2518"
            val longitude = "-75.5636"
            val expected = WeatherResult(cod = 12134)
            doReturn(expected).`when`(weatherUseCase).getWeatherByLocalization(latitude, longitude)
            cityViewModel.getWeatherByLocalization(latitude, longitude)
            verify(weatherUseCase).getWeatherByLocalization(latitude, longitude)
            Assert.assertEquals(cityViewModel.weatherResult.value, expected)
            Assert.assertEquals(cityViewModel.showCircularProgressIndicator.value, false)
        }
    }
}