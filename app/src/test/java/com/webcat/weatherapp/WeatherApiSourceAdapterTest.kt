package com.webcat.weatherapp

import com.webcat.models.WeatherResult
import com.webcat.weatherapp.datasource.api_source.WeatherApiSourceAdapter
import com.webcat.weatherapp.datasource.api_source.WeatherServices
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class WeatherApiSourceAdapterTest {
    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var weatherServices: WeatherServices
    private lateinit var weatherApiSourceAdapter: WeatherApiSourceAdapter

    @Before
    fun setUp() {
        weatherApiSourceAdapter = WeatherApiSourceAdapter(weatherServices)
    }

    @Test
    fun getWeatherByCityNameShouldReturnDataFromRepository() {
        testCoroutineRule.runBlockingTest {
            val cityName = "Medellin"
            val responseBody = WeatherResult()
            val expected = Response.success(responseBody)
            doReturn(expected).`when`(weatherServices).getWeatherByCityName(cityName)
            val result = weatherApiSourceAdapter.getWeatherByCityName(cityName)
            verify(weatherServices).getWeatherByCityName(cityName)
            Assert.assertEquals(result, responseBody)

        }
    }

    @Test
    fun getWeatherByLocationNameShouldReturnDataFromRepository() {
        testCoroutineRule.runBlockingTest {
            val latitude = "6.2518"
            val longitude = "-75.5636"
            val responseBody = WeatherResult()
            val expected = Response.success(responseBody)
            doReturn(expected).`when`(weatherServices).getWeatherByLocalization(latitude, longitude)
            val result = weatherApiSourceAdapter.getWeatherByLocalization(latitude, longitude)
            verify(weatherServices).getWeatherByLocalization(latitude, longitude)
            Assert.assertEquals(result, responseBody)

        }
    }
}