# Weather Mobile App

Weather App is an application to consult weather data, it performs the search by city name or by location, it has the respective validations for the request of permissions to the user.

## Architecture

The architecture pattern used was MVVM with Clean Architecture, seeking to decouple each of the layers of the application.

## Dependency injection

Implemented dependency injection using Hilt

## Continuous integration
The gitlab pipeline was configured to run the test and build stages.

## Objectives achieved
* Implementation of MVVM architecture.
* Use of clean architecture
* Unit testing in all layers of the application.
* Continuous integration configuration
* Implementation of dependency injection
* Clean code
